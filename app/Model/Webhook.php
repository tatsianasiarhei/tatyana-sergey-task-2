<?php
App::uses('AppModel', 'Model');


class Webhook extends AppModel
{
    const STATUS_PROCESSED   = 'processed';
    const STATUS_UNPROCESSED = 'unprocessed';

	public function saveWebhookData(array $webhookData)
	{
		try {
			$this->save($webhookData);
		} catch (Exception $e) {
			CakeLog::write('error', 'Dont save new webhook model. ' . $e->getCode() . ': '. $e->getMessage());
		}
	}

	public function getUnprocessedWebhooks()
	{
		return $this->find('all',
			[
				'conditions' =>
					[
						'Webhook.status' => Webhook::STATUS_UNPROCESSED,
					],
				'limit' => 15
			]);
	}
}
