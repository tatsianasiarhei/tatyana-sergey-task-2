<?php
App::uses('VerifyWebhook', 'Services');


class WebhooksController extends AppController
{
	public function handleWebhook(): void
	{
        $verifyWebhook = new VerifyWebhook;
        if ($verifyWebhook->verifyWebhook($this->request)) {
            $this->loadModel('Webhook');
            $this->Webhook->saveWebhookData($this->getWebhookData());
        }
	}

	private function getWebhookData(): array
    {
        return [
            'shop_domain' => $this->request->header('X-Shopify-Shop-Domain'),
            'topic' => $this->request->header('X-Shopify-Topic'),
            'body' => $this->request->input(),
            'hmac' => $this->request->header('X-Shopify-Hmac-Sha256'),
        ];
    }

	public function index(): void
	{
		$this->autoRender = false;
	}
}
