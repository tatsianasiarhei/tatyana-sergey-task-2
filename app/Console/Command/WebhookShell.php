<?php
App::uses('WebhookApiHandler', 'Services');
Configure::load('tags');
Configure::load('shopify');


class WebhookShell extends AppShell
{
	protected $webhookApiHandler;
	protected $token;
	protected $domain;
	protected $saleTag;
	protected $outOfStockTag;

	public $uses = ['Webhook'];

	public function main() {
		$tagsConfig = Configure::read('tags');
		$this->saleTag = $tagsConfig['sale'];
		$this->outOfStockTag = $tagsConfig['outOfStock'];
		$this->discounted = $tagsConfig['discounted'];

		$shopifyConfig = Configure::read('shopify');
		$this->token = $shopifyConfig['token'];
		$this->domain = $shopifyConfig['domain'];

		$this->webhookApiHandler = new WebhookApiHandler($this->domain, $this->token);

		$this->changeWebhooks($this->Webhook->getUnprocessedWebhooks());

		$this->out('Task completed!');
	}

	private function changeWebhooks(array $webhooks): void
	{
		if (!empty($webhooks)) {
			foreach ($webhooks as $webhook) {
				$webhookBody = json_decode($webhook['Webhook']['body']);

				if ($webhook['Webhook']['topic'] == "products/updated") {
					$this->webhookApiHandler->changeProductTags($webhookBody, [$this->saleTag, $this->outOfStockTag]);
				} elseif ($webhook['Webhook']['topic'] == "orders/updated") {
					$this->webhookApiHandler->changeProductsTagsInOrder($webhookBody, $this->discounted, $this->saleTag);
				}

				$this->Webhook->updateAll([
					'status' => Webhook::STATUS_PROCESSED,
				], [
					'hmac' => $webhook['Webhook']['hmac']
				]);
			}
			$this->out('No webhooks!');
		}
	}

}
