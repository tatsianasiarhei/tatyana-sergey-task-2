<?php
Configure::load('shopify');


class VerifyWebhook
{
    public function verifyWebhook($request): bool
    {
        $webhookData = $request->input();
        if (empty($webhookData)){
            CakeLog::write('error', 'Empty webhook');

            return false;
        }

        $shopifyConfig = Configure::read('shopify');
        $sharedSecret = $shopifyConfig['sharedSecret'];

        $calculatedHmac = base64_encode(hash_hmac('sha256', $webhookData, $sharedSecret, true));

        if (!hash_equals($request->header('X-Shopify-Hmac-Sha256'), $calculatedHmac)){
            CakeLog::write('error', 'Webhook fail');

            return false;
        }

        CakeLog::write('info', 'Webhook success. ' . $calculatedHmac);

        return true;
    }
}