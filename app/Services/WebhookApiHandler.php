<?php
App::uses('Shopify', 'Lib/external-api');


class WebhookApiHandler extends Shopify
{
	public function changeProductTags($product, array $disaredTags): void
	{
		foreach ($disaredTags as $disaredTag){
			if ($disaredTags == 'Sale') {
				if ($this->checkProductVariantsPrice($product->variants) && !$this->hasDisaredTag($product->tags, $disaredTag)) {
					$this->pushNewTags('product', $product, $product->tags . ', ' . $disaredTag);
				}
			} elseif ($disaredTags == 'Out of stock') {
				$inventoryItemIds = $this->getInventoryProductVariantIds($product->variants);
				$inventoryLevels = $this->get('inventory_levels', 'inventory_item_ids', $inventoryItemIds);

				if ((!$this->isInventoryItemAvailable($inventoryLevels)) && (!$this->hasDisaredTag($product, $disaredTag))) {
					$this->pushNewTags('product', $product, $product->tags . ', ' . $disaredTag);
				}
			}
		}
	}

	public function changeProductsTagsInOrder($order, $disaredTag, $saleTag): bool
	{
		if ($this->hasSaleTag($order->line_items, $saleTag) && !$this->hasDisaredTag($order, $disaredTag)) {
			$this->pushNewTags('order', $order, $disaredTag);

			return true;
		}

		return false;
	}

	protected function checkProductVariantsPrice(array $variants): bool
	{
		foreach ($variants as $variant) {
			if ($variant->compare_at_price !== null) {
				return true;
			}

			return false;
		}
	}

	protected function hasDisaredTag($productTags, $disaredTag): bool
	{
		return in_array($disaredTag, explode(', ', $disaredTag));
	}

	protected function pushNewTags($resource, object $object, $tags): void
	{
		$fields = [
			$resource => [
				'id' => $object->id,
				'tags' => $tags
			]
		];

		$this->put($resource, $object->id, $fields);
	}

	protected function getInventoryProductVariantIds($productVariants)
	{
		$inventoryItemIds = [];

		foreach ($productVariants as $productVariant) {
			if (($productVariant->inventory_item_id !== null)) {
				$inventoryItemIds[] = $productVariant->inventory_item_id;
			}
		}

		return [
			'inventory_item_ids' => implode(",", $inventoryItemIds),
		];
	}

	protected function isInventoryItemAvailable($inventory_levels)
	{
		foreach ($inventory_levels as $inventory_level) {
			if ($inventory_level->available > 0) {
				return true;
			}
		}

		return false;
	}

	protected function hasSaleTag($orderProducts, $saleTag)
	{
		foreach ($orderProducts as $orderProduct) {
			try {
				$product = $this->get('products', $orderProduct->product_id);
			} catch (Exception $e) {
				CakeLog::write('error', 'GET Products error ' . $e->getMessage());
			}

			if ($this->hasDisaredTag($product, $saleTag)){
				return true;
			}
		}

		return false;
	}
}
