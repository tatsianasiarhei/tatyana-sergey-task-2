<?php
App::uses('AbstractApi', 'Lib/external-api');

/**
 * Provides interaction with Shopify API.
 *
 * @author Polyakov Ivan
 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
 * @link http://spur-i-t.com
 * @version 3.0.2
 */
class Shopify extends AbstractApi
{
	protected $apiName = 'Shopify';
	protected $apiLimit = 250;

	protected $apiUrl = 'https://%s/admin/api/2019-10/%s';

	protected $apiResouces = array (
		'application_charges', 'application_charges/activate', 'articles/authors', 'articles/tags', 'blogs',
		'blogs/articles', 'checkouts', 'collects', 'comments', 'comments/spam', 'comments/not_spam',
		'comments/approve', 'comments/remove', 'comments/restore', 'countries', 'countries/provinces',
		'custom_collections', 'customers', 'customers/addresses', 'draft_orders', 'draft_orders/send_invoice',
		'draft_orders/complete', 'draft_orders/count', 'events', 'fulfillment_services', 'fulfillments',
		'inventory_levels', 'inventory_levels/adjust', 'inventory_levels/connect', 'inventory_levels/set',
		'locations', 'locations/inventory_levels', 'metafields', 'oauth/access_scopes', 'orders', 'orders/close',
		'orders/open', 'orders/risks', 'orders/cancel', 'orders/events', 'orders/fulfillments', 'pages', 'products',
		'products/images', 'products/variants', 'products/metafields', 'product_search_engines',
		'recurring_application_charges', 'recurring_application_charges/activate', 'redirects', 'script_tags',
		'shop', 'smart_collections', 'themes', 'themes/assets', 'transactions', 'variants', 'variants/metafields',
		'webhooks'
	);

	/**
	 * Relative Pagination Cursors
	 * @var array
	 */
	private $lastGetRequestCursors = array ();

	/**
	 * Constructor.
	 * Initializes api.
	 * @param string $shop - shop name.
	 * @param string $token - token.
	 * @return Shopify
	 */
	public function __construct( $shop, $token )
	{
		parent::__construct();
		$this->init( $shop, $token );
		$this->responseHeaders = true;
		curl_setopt( $this->client, CURLOPT_HEADER, true );

		return $this;
	}

	/**
	 * Initializes api.
	 * @param string $shop - shop name.
	 * @param string $token - token.
	 * @return Shopify
	 */
	public function init( $shop, $token )
	{
		$this->shop = $shop;
		$this->headers = array (
			"X-Shopify-Access-Token: {$token}"
		);

		return $this;
	}

	public function getAll( $resource, $idBunch = null, $params = array(), $format = 'json' )
	{
//CakeLog::write('params', print_r($params, true));
		$allItems = array();
		$params['limit'] = $this->apiLimit;
		$ids = array();
		if ( isset($params['ids']) && is_array($params['ids']) ) {
			if ( count($params['ids']) > $this->apiLimit  ) {
				$ids = array_chunk($params['ids'], $this->apiLimit);
				unset($params['ids']);
			} else {
				$params['ids'] = implode(',', $params['ids']);
			}
		}
		$page = 1;
		do {
//                $params['page'] = $page;
			if (!empty($ids)) {
				$params['ids'] = implode(',', $ids[$page-1]);
			}
			$part = $this->get($resource, $idBunch, $params, $format);
// CakeLog::write('parts', print_r($part, true));
			if (is_array($part) && !empty($part)) {
				$allItems = array_merge($allItems, $part);
			}
			$page++;
		} while(!empty($ids[$page-1]));

		return $allItems;
	}

	/**
	 * @throws Exception
	 * @see AbstractAPI::_processResponse()
	 */
	protected function _processResponse( $response, $responseHeaders )
	{
		$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
		if ( $code == 429 ) {
			CakeLog::write('sh_api', 'Shop domain: ' . $this->shop . '. Error: (429)Request limit reached, sleep 2s...');
			sleep( 2 );

			$response = $this->_processResponse(
				$this->_decodeResponse(curl_exec($this->client)),
				array() // TODO this kills $responseHeaders
			);
		}
		if ( isset ( $response->errors ) ) {
			$errors = json_encode( $response->errors );
			throw new Exception(
				$this->_message( '(' . $code . ') - ' . $errors ),
				$code
			);
		}

		if(isset($responseHeaders['Link'])) {
			$key = array_keys((array)$response)[0];
			unset($this->lastGetRequestCursors[$key]);

			$parts = explode(',', $responseHeaders['Link'], 2);

			foreach ($parts as $part) {
				$attributes = explode('; ', $part, 2);

				preg_match('/page_info=([^>&=]*)/', $attributes[0], $urlMatches);
				preg_match('/rel="(.*)"/', $attributes[1], $flagMatches);

				if(isset($urlMatches[1], $flagMatches[1])) {
					// TODO $this->client get POST fields for hash
					$this->lastGetRequestCursors[$key][$flagMatches[1]] = $urlMatches[1];
				}
			}
		}

		return $response;
	}

	/**
	 * @throws Exception
	 * @see AbstractAPI::_processNoResponse()
	 */
	protected function _processNoResponse()
	{
		$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
		if ( $code !== 200 ) {
			throw new Exception(
				$this->_message( 'No response: ' . $code )
			);
		}
		return true;
	}

	/**
	 * @see AbstractAPI::_getMethodResponse()
	 */
	protected function _getMethodResponse( $resource, $response, $idBunch )
	{
		if ( isset ( $response->count ) ) {
			$response = $response->count;
		}
		$bunch = array (
			'products' => 'product',
			'variants' => 'variant'
		);
		if ( in_array( $resource, array_keys( $bunch ) ) && $idBunch ) {
			$resource = $bunch[ $resource ];
		}
		if ( isset ( $response->$resource ) ) {
			$response = $response->$resource;
		}
		return $response;
	}

	/**
	 * Returns api key.
	 * @return string
	 */
	public function key() {
		return $this->apiKey;
	}

	/**
	 * Returns api secret.
	 * @return string
	 */
	public function secret() {
		return $this->sharedSecret;
	}

	/**
	 * Returns main theme name.
	 * @return string|null
	 */
	public function getMainTheme()
	{
		if ( $themes = $this->get( 'themes' ) ) {
			foreach ( $themes as $_theme ) {
				if ( $_theme->role == 'main' ) {
					return $_theme;
				}
			}
		}
		return null;
	}

	/**
	 * Get store information
	 * @return string|null
	 */
	public function getShop()
	{
		return $this->get('shop');
	}

	/**
	 * @param $resource
	 * @param string $flag
	 * @return |null
	 * $flag can be 'next' or 'previous'.
	 */
	public function getLastGetRequestCursor($resource, $flag = 'next')
	{

		// TODO 2+ products/get will overwrite each other

		return isset($this->lastGetRequestCursors[$resource][$flag]) ?
			$this->lastGetRequestCursors[$resource][$flag] :
			null;
	}

	/**
	 * @param $resource
	 * @return array
	 */
	public function getLastGetRequestCursors($resource)
	{
		return [
			'prev' => $this->getLastGetRequestCursor($resource, 'previous'),
			'next' => $this->getLastGetRequestCursor($resource, 'next')
		];
	}

	/**
	 * @param $resource
	 * @param $params
	 * @param null $errorCallback
	 * @return array
	 * @throws Exception
	 */
	public function getAllPaginated($resource, $params, $errorCallback = null)
	{
		$next = '';
		$items = [];

		do {
			if ($next) {
				$options = ['page_info' => $next];

				if (!empty($params['fields'])) {
					$options['fields'] = $params['fields'];
				}

				if (!empty($params['limit'])) {
					$options['limit'] = $params['limit'];
				}
			} else {
				$options = $params;
			}

			try {
				$itemsPage = $this->get($resource, null, $options);
				$items = array_merge($items, $itemsPage);
				$next = $this->getLastGetRequestCursor($resource, 'next');
			} catch (\Exception $e) {
				if (!is_callable($errorCallback)) {
					throw $e;
				}

				$errorCallback($e);
			}
		} while($next);

		return $items;
	}

	/**
	 * @param $resource
	 * @param $params
	 * @param $eachPageCallback
	 * @param null $errorCallback
	 * @throws Exception
	 */
	public function eachPage($resource, $params, $eachPageCallback, $errorCallback = null)
	{
		if (!is_callable($eachPageCallback)) {
			throw new InvalidArgumentException('eachPageCallback must be callable');
		}

		$next = '';
		do {
			if ($next) {
				$options = ['page_info' => $next];

				if (!empty($params['fields'])) {
					$options['fields'] = $params['fields'];
				}

				if (!empty($params['limit'])) {
					$options['limit'] = $params['limit'];
				}
			} else {
				$options = $params;
			}

			try {
				$itemsPage = $this->get($resource, null, $options);
				$eachPageCallback($itemsPage);
				$next = $this->getLastGetRequestCursor($resource, 'next');
			} catch (\Exception $e) {
				if (!is_callable($errorCallback)) {
					throw $e;
				}

				$errorCallback($e);
			}
		} while($next);
	}
}

