<?php

$config['tags'] = [
	'sale' => 'Sale',
	'outOfStock' => 'Out of stock',
	'discounted' => 'Discounted',
];
